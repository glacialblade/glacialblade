<?php namespace App\PostTypes;

class SamplePostType extends \App\PostTypes\Core\PostType {

	// If the post type you want to control is page you don't need to specify these Attributes Anymore.

	// Post Type Name
	protected $label    = 'Sample';
	protected $type     = 'sample';

	// Default Not Required.
	protected $icon     = 'dashicons-admin-post';
	protected $taxonomy = false;
	protected $exclude  = false;
	protected $supports = ['title', 'thumbnail', 'editor', 'page-attributes'];

	protected $metabox = [
		'Extra Fields' => [
			'id'         => 'extra_fields',
			'title'      => 'Extra Fields',
			'pages'      => ['sample'],
			'show_names' => true,
			'template'   => '',
			'fields'     => [
				[ 'name' => 'Extra Field 1', 'id' => 'extra_field_1', 'type' => 'text' ],
			]
		]
	];

	// Enqueue scripts here.
	public function scripts() {

	}

	// Custom Logic Here
	public function custom() {

	}

}